const app = require('express')();
const express = require('express');
const validator = require('validator');

const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

const emailsJson = require('./emails.json');

app.use(express.json());
const route = express.Router();
const port = process.env.PORT || 5000;
app.use('/', route);

const transporter = nodemailer.createTransport({
  port: 587,
  host: 'email-smtp.us-east-1.amazonaws.com',
  auth: {
    user: 'AKIA3F77KNJBPO7UU4HD',
    pass: 'BOvfiyoip5KM4HZ8igyiNs0B2Hqsn7/iuEg9zXlOT8PE',
  },
  secure: false,
});

transporter.use(
  'compile',
  hbs({
    // @ts-ignore
    viewEngine: 'express-handlebars',
    viewPath: './',
  })
);

route.post('/text-mail', (req, res) => {
  const {
    subject,
    text,
  } = req.body;

  emailsJson.forEach(async function (entry) {
    // @ts-ignore
    if (validator.isEmail(entry.email)) {
      if (entry.name === 'name') {
        entry.name = '';
      }
      let promise = new Promise((resolve, reject) => {
        setTimeout(
          () =>
            resolve({
              from: 'info@sobachulya.ru',
              to: entry.email,
              subject: `${subject} ${entry.name}`,
              text: text,
              template: 'main',
            }),
          1000
        );
      });

      let result = await promise;
      console.log(result);

      transporter.sendMail(result, function (err, info) {
        if (err) {
          return console.log(err);
        }
        console.log(info);
        return res.status(200).send({
          message: 'Сообщение отправлено',
          massage_id: info.massageId,
        });
      });
    }
  });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
